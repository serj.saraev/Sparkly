# Sparkly

Ссылка на бот:

https://t.me/laundrysymbolsbot

Ссылка на сайт:

https://sparklyai.tilda.ws/

## Аннотация

Суть проекта заключается в распознавании символов по уходу за одеждой. Для задачи object detection использовалась обученная
на датасете COCO модель faster_rcnn_X_101_32x8d_FPN_3x. Обучение выполнялось через библиотеку Detectron2. 

## Структура проекта
```
── .dvc
── data
│   ├── external                   <- Данные из сторонних источников
│   ├── interim                    <- Данные в промежуточной обработке
│   ├── processed                  <- Данные для обучения 
│   │   ├── train                  <- Фотографии с разметкой для стадии обучения
│   │   ├── valid                  <- Фотографии с разметкой для валидации
│   └── raw                        <- Сырые данные
├── docker
│   ├── docker-compose.yml         <-Docker compose для MLflow, PostgreSQL и S3
│   └── mlflow_image
│       └── Dockerfile             <- Dockerfile для MLflow
├── Dockerfile                     <- Dockerfile для запуска Telegram бота 
├── dvc.lock
├── dvc.yaml                       <- Файл настроек проекта в DVC
├── input <- Папка для сохранения входных фото от пользователей
├── models
│   ├── models.pt                  <- Веса моделей
│   ├── output                     <- Папка для результатов при обучении
├── notebooks                      <- Jupyter notebooks
├── README.md
├── requirements_detectron2.txt    <- Requirements для установки Detectron2 
├── requirements.txt               <- Requirements ДО установки Detectron2
└── src
    ├── bot.py  <- Скрипт для запуска Telegram бота
    ├── configs
    │   ├── detectron2_config.yml  <- Файл конфига для инифциализации модели в Detectron2
    │   └── logging.cfg.yml        <- Файл конфига для логгов
    ├── dataset
    │   └── _annotations.coco.json <- Файл с разметкой классов для регистрации датасета в Detectron2
    ├── models
    │   ├── predict.py             <- Скрипт для инференса модели детекции символов
    │   ├── static_text.py         <- Скрипт  текстами для ответа пользователям бота
    │   └── train_model.py         <- Скрипт для запуска обучения модели
```
## Требования

Python 3.8+ \
CUDA 10.2+
### Requirements

Для корректной работы Detectron2 требуется уже наличие установленного Pytorch.
Поэтому вначале устанавливаются зависимости requirements.txt и потом
requirements_detectron2.txt

## Запуск обучения

Перед запуском обучения необходимо добавить в PYTHONPATH путь в корень проекта.
Сделать это можно с помощью следующей команды:

```angular2html
export PYTHONPATH="/path/to/project"
```
Запсук обучения осуществляется следующей командой:
```angular2html
python3 src/models/train_model.py
```

### DVC
Для загрузки данных для обучения необходимо выполнить следующие команды:

```angular2html
dvc remote modify --local myremote access_key_id {ACCESS_KEY_ID}
dvc remote modify --local myremote secret_access_key {SECRET_ACCESS_KEY}
```
где {ACCESS_KEY_ID} и {SECRET_ACCESS_KEY} — идентификатор ключа и секретный ключ от S3.\
После настройки загружаем данные командой:

```angular2html
dvc pull
```
Запуск пайплайна дла обучения выполняется командой:

```angular2html
dvc exp run
```
Веса модели сохраняются в ```models/model.pt```

## Запуск Телеграм бота


Перед запуском телеграм бота убедитесь, что в PYTHONPATH добавлен путь в корень проекта. Запуск бота осуществляетя следующей командой:
```angular2html
python3 src/bot.py
```
Для работы бота необходимо добавить токен бота в переменную окружения "TOKEN". 

## Схема работы сервиса в Draw.io

https://drive.google.com/file/d/1Rj6ZoZgYkl5_yrEWUBVjdQs0ahJIiOwi/view?usp=sharing

## Имплементация в Telegram бота

Для реализации проекта мы использовали сервис Telegram. Делали мы то с помощью библиотеки aiogram — ассинхронного фреймворка для Telegram Bot API основанный на asyncio и aiohttp.

## Точность распознавания

Точность распознавания с помощью faster_rcnn_X_101_32x8d_FPN_3x. 0.73 по метрике mAP 0.5. Наша цель: верно детектировать 4 из 5 знаков по уходу за одеждой.

## Ограничения модели 

Скорость обработки на одно изобрадение ~ 40 s/img. В данынй момент модель работает только с CPU. Потребление RAM 1.7 Gb.

## Требования к разметке 

https://drive.google.com/file/d/1efcsAG5EC5_Ob3yhHSHtPOT79ESbL_1_/view?usp=share_link

## Расчет стоимости разметки датасета

Средняя скорость разметки на одно фото составляет около 1.5 минуты на одно изображение. Таким образом, на разметку датасета объемом 3000 фото составит ~ 75 часов.
