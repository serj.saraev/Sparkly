import os

import click
import mlflow
import torch
from detectron2 import model_zoo
from detectron2.config import get_cfg, CfgNode
from detectron2.data import MetadataCatalog, DatasetCatalog
from detectron2.data.datasets import register_coco_instances
from detectron2.engine import DefaultTrainer
from detectron2.engine import HookBase
from detectron2.evaluation import COCOEvaluator
from detectron2.utils.logger import setup_logger

setup_logger()


class MLflowHook(HookBase):
    """
    A custom hook class that logs artifacts, metrics, and parameters to MLflow.
    """

    def __init__(self, cfg):
        super().__init__()
        self.cfg = cfg.clone()

    def before_train(self):
        with torch.no_grad():
            mlflow.set_tracking_uri(self.cfg.MLFLOW.TRACKING_URI)
            mlflow.set_experiment(self.cfg.MLFLOW.EXPERIMENT_NAME)
            mlflow.set_tag("mlflow.note.content",
                           self.cfg.MLFLOW.RUN_DESCRIPTION)
            with mlflow.start_run(run_name=self.cfg.MLFLOW.RUN_NAME,
                                  nested=True):
                for k, v in self.cfg.items():
                    with mlflow.start_run(nested=True):
                        mlflow.log_param(k, v)

    def after_step(self):
        with torch.no_grad():
            latest_metrics = self.trainer.storage.latest()
            for k, v in latest_metrics.items():
                mlflow.log_metric(key=k, value=v[0], step=v[1])

    def after_train(self):
        with torch.no_grad():
            with open(os.path.join(self.cfg.OUTPUT_DIR,
                                   "model-config.local.yaml"), "w") as f:
                f.write(self.cfg.dump())
            mlflow.log_artifacts(self.cfg.OUTPUT_DIR)


class CocoTrainer(DefaultTrainer):
    """
    A custom trainer class that evaluates the model on the validation set
    every 'cfg.TEST.EVAL_PERIOD' iterations.
    """

    @classmethod
    def build_evaluator(cls, cfg, dataset_name, output_folder=None):
        if output_folder is None:
            os.makedirs(cfg.OUTPUT_DIR_VALIDATION_SET_EVALUATION,
                        exist_ok=True)

        return COCOEvaluator(dataset_name, distributed=False,
                             output_dir=cfg.OUTPUT_DIR_VALIDATION_SET_EVALUATION)


class Detectron2Trainer:
    def __init__(self,
                 project_name: str = "Laundry_symbols",
                 train_data: str = "data/processed/train",
                 val_data: str = "data/processed/valid",
                 output_dir_model: str = "models",
                 output_val_dir: str = "models/output",
                 max_iter: int = 1500,
                 eval_period: int = 300,
                 batch: int = 16, ):
        self.project_name = project_name
        self.train_data = train_data
        self.val_data = val_data
        self.output_dir_model = output_dir_model
        self.output_val_dir = output_val_dir
        self.max_iter = max_iter
        self.eval_period = eval_period
        self.train_name = self.project_name + "_train"
        self.val_name = self.project_name + "_val"
        self.batch = batch

        print(self.val_data)
        register_coco_instances(self.train_name, {},
                                os.path.join(self.train_data,
                                             "_annotations.coco.json"),
                                self.train_data)

        register_coco_instances(self.val_name, {},
                                os.path.join(self.val_data,
                                             "_annotations.coco.json"),
                                self.val_data)

        self.clothes_train = MetadataCatalog.get(self.train_name, )
        self.dataset_dicts_train = DatasetCatalog.get(self.train_name)

        self.clothes_valid = MetadataCatalog.get(self.val_name)
        self.dataset_dicts_valid = DatasetCatalog.get(self.val_name)

    def __config(self) -> None:
        """
        Prepares the config for Detectron2 and MLflow

        :return: None
        :rtype: None
        """
        self.cfg = get_cfg()
        self.cfg.MLFLOW = CfgNode()
        self.cfg.MLFLOW.EXPERIMENT_NAME = self.project_name
        self.cfg.MLFLOW.RUN_DESCRIPTION = f"Model's training with " \
                                          f"{self.max_iter} iterations"
        self.cfg.MLFLOW.RUN_NAME = "Training"
        self.cfg.MLFLOW.TRACKING_URI = "http://localhost:5000"

        self.cfg.merge_from_file(model_zoo.get_config_file(
            "COCO-Detection/faster_rcnn_R_50_FPN_3x.yaml"))
        self.cfg.DATASETS.TRAIN = (self.train_name,)
        self.cfg.DATASETS.TEST = (self.val_name,)
        self.cfg.DATALOADER.NUM_WORKERS = 4
        self.cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
            "COCO-Detection/faster_rcnn_R_50_FPN_3x.yaml")
        self.cfg.SOLVER.IMS_PER_BATCH = self.batch
        self.cfg.SOLVER.BASE_LR = 0.001
        self.cfg.SOLVER.MAX_ITER = 500
        self.cfg.TEST.EVAL_PERIOD = 250
        self.cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 256
        self.cfg.MODEL.ROI_HEADS.NUM_CLASSES = 32
        self.cfg.OUTPUT_DIR = self.output_dir_model
        self.cfg.OUTPUT_DIR_VALIDATION_SET_EVALUATION = self.output_val_dir
        self.mlflow_hook = MLflowHook(self.cfg)

        os.makedirs(self.cfg.OUTPUT_DIR, exist_ok=True)
        os.makedirs(self.cfg.OUTPUT_DIR_VALIDATION_SET_EVALUATION,
                    exist_ok=True)

    def __train_model(self) -> None:
        """
        Start training model

        :return: None
        :rtype: None
        """
        self.trainer = CocoTrainer(self.cfg)
        self.trainer.register_hooks(hooks=[self.mlflow_hook])
        self.trainer.resume_or_load(resume=False)
        self.__model_save()
        self.trainer.train()

    def __model_save(self) -> None:
        """
        Save model as Pytorch model

        :return: None
        :rtype: None
        """
        torch.save(self.trainer.model, self.output_dir_model + "/models.pt")

    def __call__(self):
        self.__config()
        self.__train_model()


@click.command()
@click.option("--project_name", type=click.STRING, default="Laundry_symbols", )
@click.option("--train_data", type=click.Path(exists=True),
              default="data/processed/train", )
@click.option("--val_data", type=click.Path(exists=True),
              default="data/processed/valid")
@click.option("--output_dir_model", type=click.Path(exists=False),
              default="models")
@click.option("--output_val_dir", type=click.Path(exists=False),
              default="models/output")
@click.option("--max_iter", type=click.INT, default=1500)
@click.option("--eval_period", type=click.INT, default=300)
@click.option("--batch", type=click.INT, default=16)
def train(project_name: str,
          train_data: str,
          val_data: str,
          output_dir_model: str,
          output_val_dir: str,
          max_iter: int,
          eval_period: int,
          batch: int):
    model = Detectron2Trainer(project_name=project_name,
                              train_data=train_data,
                              val_data=val_data,
                              output_dir_model=output_dir_model,
                              output_val_dir=output_val_dir,
                              max_iter=max_iter,
                              eval_period=eval_period,
                              batch=batch, )
    model()


if __name__ == "__main__":
    train()
